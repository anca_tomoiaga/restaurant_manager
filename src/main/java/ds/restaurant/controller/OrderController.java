package ds.restaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ds.restaurant.dto.OrderFDTO;
import ds.restaurant.entities.Order;
import ds.restaurant.service.OrderService;

@RestController
@RequestMapping("/order")
public class OrderController {
	@Autowired
	private OrderService orderService;
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Order> getAllOrders() {
		return orderService.getAllOrders();
	}
	
	@RequestMapping(value = "/{idOrder}", method = RequestMethod.GET)
	public Order markAsReady(@PathVariable("idOrder") int idOrder) {
		return orderService.markAsReady(idOrder);
	} 
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public OrderFDTO insertLogin(@RequestBody OrderFDTO orderFDTO) {
		return orderService.insertOrder(orderFDTO);
	}
}

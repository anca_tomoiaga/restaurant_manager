package ds.restaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ds.restaurant.dto.LoginDTO;
import ds.restaurant.entities.Login;
import ds.restaurant.service.LoginService;

@RestController
@RequestMapping("/logins")
public class LoginController {
	
	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Login> getAllLogins() {
		return loginService.getAllLogins();
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String insertLogin(@RequestBody LoginDTO login) {
		return loginService.insertLogin(login);
	}

	@RequestMapping(value = "/{idLogin}", method = RequestMethod.GET)
	public Login getLoginById(@PathVariable("idLogin") int idLogin) {
		return loginService.getLoginById(idLogin);
	} 
	
	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	public Login verifyLogin(@RequestBody LoginDTO login) {
		return loginService.getUserByUsernameAndPassword(login);
	}

}

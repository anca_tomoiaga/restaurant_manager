package ds.restaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ds.restaurant.entities.Client;
import ds.restaurant.service.ClientService;

@RestController
@RequestMapping("/client")
public class ClientController {
	@Autowired
	private ClientService clientService;
	
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Client> getAllOrderedMenus() {
		return clientService.getAllClients();
	}
}

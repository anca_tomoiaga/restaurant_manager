package ds.restaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ds.restaurant.dto.OrderedMenuDTO;
import ds.restaurant.entities.Order;
import ds.restaurant.entities.OrderedMenu;
import ds.restaurant.service.OrderedMenuService;

@RestController
@RequestMapping("/orderedMenus")
public class OrderedMenuController {
	@Autowired
	private OrderedMenuService orderedMenuService;
	
	
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public OrderedMenu insertLogin(@RequestBody OrderedMenuDTO orderedMenuDTO) {
		return orderedMenuService.insertOrderedMenu(orderedMenuDTO);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<OrderedMenu> getAllOrderedMenus() {
		return orderedMenuService.getAllOrderedMenus();
	}
}

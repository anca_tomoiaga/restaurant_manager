package ds.restaurant.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="client")
public class Client {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_client")
	private Integer idClient;
	private String name;
	private double cost;
	private int nrOfOrders;
	
	public Integer getIdClient() {
		return idClient;
	}
	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}
	public String getName() {
		return name;
	}
	public void setName(String nsme) {
		this.name = nsme;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public int getNrOfOrders() {
		return nrOfOrders;
	}
	public void setNrOfOrders(int nrOfOrders) {
		this.nrOfOrders = nrOfOrders;
	}

}

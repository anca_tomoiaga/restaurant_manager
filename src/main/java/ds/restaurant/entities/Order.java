package ds.restaurant.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="order1")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_order")
	private Integer idOrder;
	private int id_table;
	
	@ManyToOne
	@JoinColumn(name = "id_client")
	private Client client;
	
	@ManyToOne
	@JoinColumn(name = "id_login")
	private Login login;
	private boolean isBilled;
	private boolean isREady;
	public boolean isBilled() {
		return isBilled;
	}
	public void setBilled(boolean isBilled) {
		this.isBilled = isBilled;
	}
	public boolean isREady() {
		return isREady;
	}
	public void setREady(boolean isREady) {
		this.isREady = isREady;
	}
	public Integer getIdOrder() {
		return idOrder;
	}
	public void setIdOrder(Integer id_order) {
		this.idOrder = id_order;
	}
	public int getId_table() {
		return id_table;
	}
	public void setId_table(int id_table) {
		this.id_table = id_table;
	}
	
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}

}

package ds.restaurant.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ds.restaurant.entities.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
	
	public Order findByIdOrder(int id_order);

}

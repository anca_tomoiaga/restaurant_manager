package ds.restaurant.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ds.restaurant.entities.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
	
	public Client findByIdClient(int idClient);

}

package ds.restaurant.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ds.restaurant.entities.Login;

@Repository
public interface LoginRepository extends JpaRepository<Login, Integer>{
	
	public Login findByIdLogin(int idLogin);
	
	public List<Login> findAll();
	
	public Login findByUsernameAndPassword(String username, String password);
}

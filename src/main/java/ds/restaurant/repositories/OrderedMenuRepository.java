package ds.restaurant.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ds.restaurant.entities.OrderedMenu;

public interface OrderedMenuRepository extends JpaRepository<OrderedMenu, Integer> {

}

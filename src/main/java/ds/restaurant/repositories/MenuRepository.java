package ds.restaurant.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ds.restaurant.entities.Menu;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Integer> {

	public Menu findByIdMenu(int id_menu);
}

package ds.restaurant.dto;

public class ClientDTO {
	private int id_client;
	private String name;
	private double cost;
	private int nrOfOrders;
	
	public ClientDTO(){
		
	}
	
	public ClientDTO(int id_client, String name, double cost, int nrOfOrders){
		this.id_client = id_client;
		this.name = name;
		this.cost = cost;
		this.nrOfOrders = nrOfOrders;
	}
	
	public int getId_client() {
		return id_client;
	}
	public void setId_client(int id_client) {
		this.id_client = id_client;
	}
	public String getName() {
		return name;
	}
	public void setName(String nsme) {
		this.name = nsme;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public int getNrOfOrders() {
		return nrOfOrders;
	}
	public void setNrOfOrders(int nrOfOrders) {
		this.nrOfOrders = nrOfOrders;
	}

}

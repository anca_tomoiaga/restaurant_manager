package ds.restaurant.dto;

public class LoginDTO {
	
	private int idLogin;
	
	private String username;

	private String password;
	
	private String role;
	
	public LoginDTO(){
		
	}

	public LoginDTO(int idLogin, String username, String password, String role) {
		this.idLogin = idLogin;
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public int getIdLogin() {
		return idLogin;
	}

	public void setIdLogin(int idLogin) {
		this.idLogin = idLogin;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
}

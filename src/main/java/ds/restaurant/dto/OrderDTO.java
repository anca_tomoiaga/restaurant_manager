package ds.restaurant.dto;

import ds.restaurant.entities.Client;
import ds.restaurant.entities.Login;

public class OrderDTO {
	private int id_order;
	private int id_table;
	private Client client;
	private Login login;
	private boolean isBilled;
	private boolean isREady;
	public boolean isBilled() {
		return isBilled;
	}
	public void setBilled(boolean isBilled) {
		this.isBilled = isBilled;
	}
	public boolean isREady() {
		return isREady;
	}
	public void setREady(boolean isREady) {
		this.isREady = isREady;
	}
	public int getId_order() {
		return id_order;
	}
	public void setId_order(int id_order) {
		this.id_order = id_order;
	}
	public int getId_table() {
		return id_table;
	}
	public void setId_table(int id_table) {
		this.id_table = id_table;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	

}

package ds.restaurant.dto;

import ds.restaurant.entities.Menu;
import ds.restaurant.entities.Order;

public class OrderedMenuDTO {
	private int id_Ordered_menu;
	private Order order;
	private Menu menu;
	private int quantity;
	private double price;
	public int getId_Ordered_menu() {
		return id_Ordered_menu;
	}
	public void setId_Ordered_menu(int id_Ordered_menu) {
		this.id_Ordered_menu = id_Ordered_menu;
	}
	
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

}

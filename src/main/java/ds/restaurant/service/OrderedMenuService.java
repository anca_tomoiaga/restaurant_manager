package ds.restaurant.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ds.restaurant.dto.OrderedMenuDTO;
import ds.restaurant.entities.Login;
import ds.restaurant.entities.OrderedMenu;
import ds.restaurant.repositories.MenuRepository;
import ds.restaurant.repositories.OrderRepository;
import ds.restaurant.repositories.OrderedMenuRepository;

@Service
public class OrderedMenuService {
	
	@Autowired
	private MenuRepository menuRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private OrderedMenuRepository orderedMenuRepository;
	
	public OrderedMenu insertOrderedMenu(OrderedMenuDTO orderedMenuDTO){
		OrderedMenu orderedMenu = new OrderedMenu();
		
		System.out.println(orderedMenuDTO.getMenu());
		
		int idMenu = orderedMenuDTO.getMenu().getIdMenu();
		
		orderedMenu.setMenu(menuRepository.findByIdMenu(idMenu));
		orderedMenu.setOrder(orderRepository.findByIdOrder(orderedMenuDTO.getOrder().getIdOrder()));
		orderedMenu.setPrice(orderedMenuDTO.getPrice());
		orderedMenu.setQuantity(orderedMenuDTO.getQuantity());
		
		orderedMenuRepository.save(orderedMenu);
		
		return orderedMenu;
	}
	
	public List<OrderedMenu> getAllOrderedMenus(){
		return orderedMenuRepository.findAll();
	}


}

package ds.restaurant.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ds.restaurant.dto.OrderFDTO;
import ds.restaurant.entities.Client;
import ds.restaurant.entities.Login;
import ds.restaurant.entities.Menu;
import ds.restaurant.entities.Order;
import ds.restaurant.entities.OrderedMenu;
import ds.restaurant.repositories.ClientRepository;
import ds.restaurant.repositories.LoginRepository;
import ds.restaurant.repositories.MenuRepository;
import ds.restaurant.repositories.OrderRepository;
import ds.restaurant.repositories.OrderedMenuRepository;

@Service
public class OrderService {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private MenuRepository menuRepository;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private OrderedMenuRepository orderedMenuRepository;
	
/*	public Login getLoginById(int idLogin){
		Login login = loginRepository.findByIdLogin(idLogin);
		if (login == null){
			throw new ResourceNotFoundException(Login.class.getSimpleName());
		}
		return login;
	}*/
	
	public Order markAsReady(int id_order){
		Order order = orderRepository.findByIdOrder(id_order);
		order.setREady(true);
		orderRepository.save(order);
		return order;
	}
	
	public List<Order> getAllOrders(){
		return orderRepository.findAll();
	}

	public OrderFDTO insertOrder(OrderFDTO orderFDTO) {
		Menu menu = menuRepository.findByIdMenu(orderFDTO.getIdMenu());
		Client client = clientRepository.findByIdClient(orderFDTO.getIdClient());
		Login login = loginRepository.findByIdLogin(5);
		
		int quantity = orderFDTO.getQuantity();
		
		Order order = new Order();
		order.setId_table(orderFDTO.getTableNo());
		order.setClient(client);
		order.setLogin(login);
		order.setBilled(false);
		order.setREady(false);
		
		order = orderRepository.save(order);
		
		OrderedMenu orderedMenu = new OrderedMenu();
		orderedMenu.setMenu(menu);
		orderedMenu.setOrder(order);
		orderedMenu.setQuantity(quantity);
		orderedMenu.setPrice(menu.getPrice() * quantity);
		
		orderedMenuRepository.save(orderedMenu);
		
		return orderFDTO;
	}

//	public String insertLogin(LoginDTO loginDTO) {
//		Login login = new Login();
//		login.setUsername(loginDTO.getUsername());
//		login.setPassword(loginDTO.getPassword());
//		login.setRole(loginDTO.getRole());
//		loginRepository.save(login);
//		return "OK";
//	}
//	
//	public Login getUserByUsernameAndPassword(LoginDTO loginDTO) {
//		Login login = new Login();
//		login.setUsername(loginDTO.getUsername());
//		login.setPassword(loginDTO.getPassword());
//		return loginRepository.findByUsernameAndPassword(login.getUsername(), login.getPassword());
//	}

}

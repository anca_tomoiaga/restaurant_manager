package ds.restaurant.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ds.restaurant.dto.LoginDTO;
import ds.restaurant.entities.Login;
import ds.restaurant.errorhandling.ResourceNotFoundException;
import ds.restaurant.repositories.LoginRepository;

@Service
public class LoginService {
	
	@Autowired
	private LoginRepository loginRepository;
	
	public Login getLoginById(int idLogin){
		Login login = loginRepository.findByIdLogin(idLogin);
		if (login == null){
			throw new ResourceNotFoundException(Login.class.getSimpleName());
		}
		return login;
	}
	
	public List<Login> getAllLogins(){
		return loginRepository.findAll();
	}

	public String insertLogin(LoginDTO loginDTO) {
		Login login = new Login();
		login.setUsername(loginDTO.getUsername());
		login.setPassword(loginDTO.getPassword());
		login.setRole(loginDTO.getRole());
		loginRepository.save(login);
		return "OK";
	}
	
	public Login getUserByUsernameAndPassword(LoginDTO loginDTO) {
		Login login = new Login();
		login.setUsername(loginDTO.getUsername());
		login.setPassword(loginDTO.getPassword());
		return loginRepository.findByUsernameAndPassword(login.getUsername(), login.getPassword());
	}

}
